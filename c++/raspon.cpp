#include <iostream>  // komande cin i cout sad postoje
using namespace std; // uvik isto, standard namespace

int main()
{
    int broj;               // int = intiger = broj bez decimalnih mista
    cout << "Unesi broj: "; // cout = console output = ispis << strelice prema cout
    cin >> broj;            // cin = console input >> prema varijabli
    // && = i || = ili
    if (broj >= 1 && broj <= 100)
    {
        cout << "Broj se nalazi izmedu 1 i 100" << endl;
    }
    else
    {
        cout << "Broj se ne nalazi izmedu 1 i 100" << endl;
    }

    return 0;
}